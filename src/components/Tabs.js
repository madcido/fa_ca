import { changePage, pages } from './pages';
import { holdSlider, unholdSlider, dragSlider } from '../helpers/slider';

export default function Tabs() {
    function closeMenu() {
        menuTabs.classList.add('no-show');
        document.removeEventListener('click', closeMenu);
    }

    function toggleMenu(e) {
        e.stopPropagation();
        menuTabs.classList.toggle('no-show');
        document.addEventListener('click', closeMenu);
    }

    const tabs = document.createElement('div');
    tabs.className = 'header__tabs row';
    tabs.addEventListener('mousedown', holdSlider);
    tabs.addEventListener('mouseleave', unholdSlider);
    tabs.addEventListener('mouseup', unholdSlider);
    tabs.addEventListener('mousemove', dragSlider);

    pages.forEach((page, index) => {
        const tab = document.createElement('div');
        tab.className = 'header__tab';
        tab.innerHTML = page.name;
        tab.addEventListener('click', () => changePage(index));

        tabs.appendChild(tab);
    });

    const menu = document.createElement('div');
    menu.classList = 'header__menu header__tab';
    menu.innerHTML = '...';
    menu.addEventListener('click', toggleMenu);
    tabs.appendChild(menu);

    const menuTabs = document.createElement('div');
    menuTabs.classList = 'header__menu-tabs no-show';
    menu.appendChild(menuTabs);

    const tabsContainer = document.createElement('div');
    tabsContainer.classList = 'header__tabs-container';
    tabsContainer.appendChild(tabs);

    return tabsContainer;
}
