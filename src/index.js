import './styles/index.sass';
import Header from './components/Header';
import { changePage } from './components/pages';
import { responsiveTabs } from './helpers/dom-helpers';

document.body.prepend(Header());
changePage(0);

window.addEventListener('resize', responsiveTabs);
responsiveTabs();

// function toggleLabel() {
//     document.querySelector('.appended-label').classList.toggle('no-show');
// }
//
//
// const label = document.createElement('span');
// label.classList = 'appended-label no-show';
// label.innerHTML = 'hello';
//
// document.querySelector('.profile__info--followers').appendChild(label);
//
// document.querySelector('.profile__info--followers').addEventListener('mouseover', toggleLabel);
// document.querySelector('.profile__info--followers').addEventListener('mouseleave', toggleLabel);
