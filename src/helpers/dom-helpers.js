function childrenWidth(node) {
    let width = 0;
    [...node.children].map(c => width += c.offsetWidth + 25);
    return width;
}

function responsiveTabs() {
    const tabs = document.querySelector('.header__tabs');
    const menu = document.querySelector('.header__menu');
    const menuTabs = document.querySelector('.header__menu-tabs');
    if (window.innerWidth > 768) {
        if (tabs.offsetWidth < childrenWidth(tabs)) {
            menu.style.display = 'block';
            while (tabs.offsetWidth < childrenWidth(tabs)) {
                menuTabs.prepend(tabs.lastChild.previousSibling);
            }
        } else {
            if (menuTabs.firstChild) {
                if (menuTabs.firstChild.offsetWidth + 25 + childrenWidth(tabs) < tabs.offsetWidth) {
                    tabs.insertBefore(menuTabs.firstChild, tabs.lastChild);
                }
                if (!menuTabs.firstChild) {
                    menu.style.display = 'none';
                    menuTabs.classList.add('no-show');
                }
            }
        }
    } else {
        while (menuTabs.firstChild) {
            tabs.insertBefore(menuTabs.firstChild, tabs.lastChild);
        }
        menu.style.display = 'none';
        menuTabs.classList.add('no-show');
    }
}

export { childrenWidth, responsiveTabs };
